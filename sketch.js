function setup() {
  createCanvas(window.outerWidth, window.innerHeight);
  background("rgb(97, 216, 255)");
}
function draw() {
  fill(255);
  circle(mouseX, mouseY, 100);
  strokeWeight(0.75);
}
