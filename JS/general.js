const d1 = document.querySelector(".page1").offsetTop;
const d2 = document.querySelector(".page2").offsetTop;
const d3 = document.querySelector(".page3").offsetTop;

function myFunction() {
  if (document.documentElement.scrollTop < d2) {
    if (!document.querySelector(".nav1").classList.contains("active")) {
      document.querySelector(".nav1").classList.add("active");
    }
    if (document.querySelector(".nav2").classList.contains("active")) {
      document.querySelector(".nav2").classList.remove("active");
    }
  } else if (document.documentElement.scrollTop > d2) {
    if (!document.querySelector(".nav2").classList.contains("active")) {
      document.querySelector(".nav2").classList.add("active");
    }
    if (document.querySelector(".nav1").classList.contains("active")) {
      document.querySelector(".nav1").classList.remove("active");
    }
  }
}

var a = document.querySelector(".navbar-nav");
var arr = [].slice.call(a.children);
arr.forEach((el) => {
  el.addEventListener("click", myFunction);
});

console.log(arr);

window.onscroll = function () {
  myFunction();
};
